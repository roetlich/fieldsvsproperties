# Benchmarks to compare the performance of fields and properties in C#

To run the benchmarks, use:
```bash
dotnet run -c Release -p src/FieldsVsProperties/
```

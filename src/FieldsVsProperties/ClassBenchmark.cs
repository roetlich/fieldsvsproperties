using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using System.Runtime.CompilerServices;

namespace FieldsVsProperties
{
    class FieldClass {
        public int Field;

        public FieldClass(int n)
        {
            Field = n;
        }
    }
    class PropertyClass {
        public int Property {get; set;}

        public PropertyClass(int n)
        {
            Property = n;
        }
    }
    class VirtualPropertyClass {

        public virtual int VProperty {get; set;}

        public VirtualPropertyClass(int n)
        {
            VProperty = n;
        }
    }
    class NoOptimizationPropertyClass {


        public int NoOptimizationProperty
        {
            [MethodImpl(MethodImplOptions.NoOptimization)] get;
            [MethodImpl(MethodImplOptions.NoOptimization)] set;
        }

        public NoOptimizationPropertyClass(int n)
        {
            NoOptimizationProperty = n;
        }
    }
    class NoInlinePropertyClass {


        public int NoInlineProperty
        {
            [MethodImpl(MethodImplOptions.NoInlining)] get;
            [MethodImpl(MethodImplOptions.NoInlining)] set;
        }

        public NoInlinePropertyClass(int n)
        {
            NoInlineProperty = n;
        }
    }


    public class ClassBenchmark
    {
        FieldClass[] fieldObjects = new FieldClass[] {
            new FieldClass(5),
            new FieldClass(5),
            new FieldClass(5),
            new FieldClass(5),
            new FieldClass(5),
            new FieldClass(5),
        };
        PropertyClass[] propertyObjects = new PropertyClass[] {
            new PropertyClass(5),
            new PropertyClass(5),
            new PropertyClass(5),
            new PropertyClass(5),
            new PropertyClass(5),
            new PropertyClass(5),
        };
        VirtualPropertyClass[] vPropertyObjects = new VirtualPropertyClass[] {
            new VirtualPropertyClass(5),
            new VirtualPropertyClass(5),
            new VirtualPropertyClass(5),
            new VirtualPropertyClass(5),
            new VirtualPropertyClass(5),
            new VirtualPropertyClass(5),
        };
        NoOptimizationPropertyClass[] NoOptimizationPropertyObjects = new NoOptimizationPropertyClass[] {
            new NoOptimizationPropertyClass(5),
            new NoOptimizationPropertyClass(5),
            new NoOptimizationPropertyClass(5),
            new NoOptimizationPropertyClass(5),
            new NoOptimizationPropertyClass(5),
            new NoOptimizationPropertyClass(5),
        };
        NoInlinePropertyClass[] NoInlinePropertyObjects = new NoInlinePropertyClass[] {
            new NoInlinePropertyClass(5),
            new NoInlinePropertyClass(5),
            new NoInlinePropertyClass(5),
            new NoInlinePropertyClass(5),
            new NoInlinePropertyClass(5),
            new NoInlinePropertyClass(5),
        };


        // Read Benchmarks
        [Benchmark]
        public int ReadFields()
        {
            int sum = 0;
            foreach(var obj in fieldObjects)
            {
                sum += obj.Field;
            }
            return sum;
        }
        [Benchmark]
        public int ReadProperties()
        {
            int sum = 0;
            foreach(var obj in propertyObjects)
            {
                sum += obj.Property;
            }
            return sum;
        }
        [Benchmark]
        public int ReadVirtualProperties()
        {
            int sum = 0;
            foreach(var obj in vPropertyObjects)
            {
                sum += obj.VProperty;
            }
            return sum;
        }
        [Benchmark]
        public int ReadUnoptimizedProperties()
        {
            int sum = 0;
            foreach(var obj in NoOptimizationPropertyObjects)
            {
                sum += obj.NoOptimizationProperty;
            }
            return sum;
        }
        [Benchmark]
        public int ReadNoInlineProperties()
        {
            int sum = 0;
            foreach(var obj in NoInlinePropertyObjects)
            {
                sum += obj.NoInlineProperty;
            }
            return sum;
        }

        // Write Benchmarks
        [Benchmark]
        public void WriteFields()
        {
            int num = 5;
            foreach(var obj in fieldObjects)
            {
                obj.Field = num;
            }
        }
        [Benchmark]
        public void WriteProperties()
        {
            int num = 5;
            foreach(var obj in propertyObjects)
            {
                obj.Property = num;
            }
        }
        [Benchmark]
        public void WriteVirtualProperties()
        {
            int num = 5;
            foreach(var obj in vPropertyObjects)
            {
                obj.VProperty = num;
            }
        }
        [Benchmark]
        public void WriteUnoptimizedProperties()
        {
            int num = 5;
            foreach(var obj in NoOptimizationPropertyObjects)
            {
                obj.NoOptimizationProperty = num;
            }
        }
        [Benchmark]
        public void WriteNoInlineProperties()
        {
            int num = 5;
            foreach(var obj in NoInlinePropertyObjects)
            {
                obj.NoInlineProperty = num;
            }
        }
    }
}

using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace FieldsVsProperties
{
    struct FieldStruct {
        public int Field;

        public FieldStruct(int n)
        {
            Field = n;
        }
    }
    struct PropertyStruct {
        public int Property {get; set;}

        public PropertyStruct(int n)
        {
            Property = n;
        }
    }

    public class StructBenchmark
    {
        FieldStruct[] fieldObjects = new FieldStruct[] {
            new FieldStruct(5),
            new FieldStruct(5),
            new FieldStruct(5),
            new FieldStruct(5),
            new FieldStruct(5),
            new FieldStruct(5),
        };
        PropertyStruct[] propertyObjects = new PropertyStruct[] {
            new PropertyStruct(5),
            new PropertyStruct(5),
            new PropertyStruct(5),
            new PropertyStruct(5),
            new PropertyStruct(5),
            new PropertyStruct(5),
        };

        // Read Benchmarks
        [Benchmark]
        public int ReadFields()
        {
            int sum = 0;
            foreach(var obj in fieldObjects)
            {
                sum += obj.Field;
            }
            return sum;
        }
        [Benchmark]
        public int ReadProperties()
        {
            int sum = 0;
            foreach(var obj in propertyObjects)
            {
                sum += obj.Property;
            }
            return sum;
        }
        [Benchmark]
        public int ReadFieldsFromStack()
        {
            FieldStruct a = new FieldStruct(2);
            FieldStruct b = new FieldStruct(2);
            FieldStruct c = new FieldStruct(2);
            FieldStruct d = new FieldStruct(2);
            FieldStruct e = new FieldStruct(2);
            FieldStruct f = new FieldStruct(2);
            int sum = 0;
            
            sum += a.Field;
            sum += b.Field;
            sum += c.Field;
            sum += d.Field;
            sum += e.Field;
            sum += f.Field;

            return sum;
        }
        [Benchmark]
        public int ReadPropertiesFromStack()
        {
            PropertyStruct a = new PropertyStruct(2);
            PropertyStruct b = new PropertyStruct(2);
            PropertyStruct c = new PropertyStruct(2);
            PropertyStruct d = new PropertyStruct(2);
            PropertyStruct e = new PropertyStruct(2);
            PropertyStruct f = new PropertyStruct(2);
            int sum = 0;
            
            sum += a.Property;
            sum += b.Property;
            sum += c.Property;
            sum += d.Property;
            sum += e.Property;
            sum += f.Property;

            return sum;
        }

        // Write Benchmarks
        [Benchmark]
        public void WriteFields()
        {
            int num = 5;
            for(int i = 0; i < fieldObjects.Length; i++)
            {
                fieldObjects[i].Field = num;
            }
        }
        [Benchmark]
        public void WriteProperties()
        {
            int num = 5;
            for(int i = 0; i < propertyObjects.Length; i++)
            {
                propertyObjects[i].Property = num;
            }
        }
    }
}

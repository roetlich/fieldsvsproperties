using BenchmarkDotNet.Attributes;


public class ClassInternalBenchmark
{
	private const long NumValues = 1000000000;
	public long SumAuto { get; private set; }
	private long sum;
 
	[Benchmark]
	public void SumProperty()
	{
		for (long i = 0; i < NumValues; ++i)
		{
			SumAuto += i;
		}
	}
	[Benchmark]
	public void SumField()
	{
		for (long i = 0; i < NumValues; ++i)
		{
			sum += i;
		}
	}
	 
	[Benchmark]
	public void SumLocal()
	{
		long localSum = 0;
		for (long i = 0; i < NumValues; ++i)
		{
			localSum += i;
		}
		sum = localSum;
	}
}